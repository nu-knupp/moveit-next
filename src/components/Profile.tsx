import { useContext } from 'react'
import { ChallengeContext } from '../contexts/ChallengeContext'
import styles from '../styles/components/Profile.module.css'

export function Profile() {
    const { level } = useContext(ChallengeContext);
    return (
        <div className={styles.profileContainer}>
            <img src="https://media-exp1.licdn.com/dms/image/C4D03AQGuyn3hSJ475A/profile-displayphoto-shrink_200_200/0/1576623457742?e=1619654400&v=beta&t=4vv-reHlUYR2oeZWwu-1sUwQwaI1AvHvawxMKc4ZOLE" 
                alt="Nubia Knupp" 
            />
            <div>
                <strong>Núbia Knupp</strong>
                <p>
                    <img src="icons/level.svg" alt="Level" />
                    Level {level}
                </p>
            </div>
        </div>
    )
}